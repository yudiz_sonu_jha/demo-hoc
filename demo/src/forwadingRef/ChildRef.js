import React from 'react'

const ChildRef = React.forwardRef((props,ref)=>(
  
  <div className='childref'>
        <h4>ChildRef:</h4>
        <div className='input'>
          <input ref={ref} type="text" placeholder='enter value'/>
        </div>
       
    </div>    
))

export default ChildRef;