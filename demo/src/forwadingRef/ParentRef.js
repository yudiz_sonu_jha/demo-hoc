import React, { Component } from 'react'
import ChildRef from "./ChildRef"
// {}
export default class Child extends Component {
    constructor(props){
        super(props);

        this.myRef = React.createRef();
    }
    onClick = () =>{
        this.myRef.current.focus();
        this.myRef.current.placeholder = "Button CLicked"
        console.log("value",this.myRef.current.value)
        
    };
  render() {
    return (
      <div>
          <h1 className='frd'>Forwarding Ref Demo</h1>
        <div className='parentref'>
          <h4>parent Ref:</h4>
          <ChildRef ref={this.myRef}/>
          <button className='btn' onClick={this.onClick}>Click</button>
        </div>

      </div>
     
    )
  }
}
