import React, {  useState } from 'react'
function HOC() {
  return (
    <div className="App">
      <h1>HOC </h1>
      <HOCNum com={Counter} />
      
    </div>
  );
}
function HOCNum(props)
{
  return <h2>Number<props.com /></h2>
}

function Counter()
{
  const [count,setCount]=useState(0)
  return<div>
    <h3>{count}</h3>
    <button onClick={()=>setCount(count+1)}>Increment</button>
    <button onClick={()=>setCount(count-1)}>Decrement</button>
  </div>
}

export default HOC;