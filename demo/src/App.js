import './App.css';
import React from 'react'
import { Switch, Route } from 'react-router-dom';

import ParentRef from "./forwadingRef/ParentRef";

import HOC from './HOC'
import { BrowserRouter } from 'react-router-dom/cjs/react-router-dom';
import FragmentDemo from './component/FragementDemo';
import ParentComponent from './portal/ParentComponent';

function App() {
  return (
    
    <>      
      <ParentRef/> 
    <BrowserRouter>
      <Switch>
        <Route exact path="/hoc" component={HOC} />
        <Route exact path="/" component={FragmentDemo} />
      </Switch>
    </BrowserRouter>  
    <h1 className='prt'>this is portal component Demo</h1>
    <ParentComponent />
    </>
  )
}

export default App;