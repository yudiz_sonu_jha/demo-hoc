import React, { Component } from "react";
import Columns from "./Column";
export default class TableDemo extends Component {
  render() {
    return (
      <table>
        <tr>
          <Columns />
        </tr>
      </table>
    );
  }
}